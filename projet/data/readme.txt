Advertising
R Visualization: advertising through different media.  
The dataset contains statistics about the sales of a product in 200 different markets, together with advertising budgets in each of these markets for different media channels: TV, radio and newspaper.
The sales are in thousands of units and the budget is in thousands of dollars.

Python classification: 
In this project we will be working with a fake advertising data set, indicating whether or not a particular internet user clicked on an Advertisement. You will try to create a model that will predict whether or not they will click on an ad based off the features of that user.
This data set contains the following features:
•	'Daily Time Spent on Site': consumer time on site in minutes
•	'Age': cutomer age in years
•	'Area Income': Avg. Income of geographical area of consumer
•	'Daily Internet Usage': Avg. minutes a day consumer is on the internet
•	'Ad Topic Line': Headline of the advertisement
•	'City': City of consumer
•	'Male': Whether or not consumer was male
•	'Country': Country of consumer
•	'Timestamp': Time at which consumer clicked on Ad or closed window
•	'Clicked on Ad': 0 or 1 indicated clicking on Ad

NLP analysis: Super Bowl advertising
I thought it might be interesting to look at advertising trends across the most watched US program, the Super Bowl. 
Content: it's a CSV of all advertisements shown during the Super Bowl across the years from 1967 to 2020.
